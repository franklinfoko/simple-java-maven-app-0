FROM openjdk:11
ADD target/m-erpsync.jar m-erpsync.jar
EXPOSE 9007
ENTRYPOINT ["java", "-jar", "m-erpsync.jar"]
